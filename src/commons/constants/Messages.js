'use strict';

module.exports = {
  FIELD_REQUIRED: 'Field "{{param}}" is required',
  FIELD_ARRAY: 'Field "{{param}}" must be an array',
  FIELD_ARRAY_OBJECT: 'Field "{{param}}" must be an array of objects',
  FIELD_OBJECT: 'Field "{{param}}" must be an object',
  FIELD_BOOLEAN: 'Field "{{param}}" must be a boolean value',
  NO_RESULT: 'No results found',
  SUCCESS: 'Operation successfully performed',
  INVALID_PARAMS: 'Invalid params',
  ERROR_ON_SAVE: 'Error on try to save data',
  ERROR_ON_UPDATE: 'Error on try to update data',
  UPDATE_NOT_OCURRED: 'Update not effective',
  ERROR_ON_DELETE: 'Error on try to delete data',
  DATA_NOT_FOUND: 'Data not found',
  DUPLICATED: 'The field "{{name}}" must be a unique value'
};
